# Write a function that, given a list of lists, returns the list with the fewest integers.

# Sample inputs/outputs:

# numbers	output
# []	None
# [[1, 2, 3], [1]]	[1]
# [[1, 2, 3], [3, 2], [1, 2, 11, 200]]	[3, 2]

lol = [[1, 2, 3], [1, 2, -11, 200],[0,0,0,0,0,0,0,0] ]

def find_shortest(list_of_lists):
    shortest = list_of_lists[0]
    for list in list_of_lists:
        if len(list) == 0:
            return None
        else:
            if len(list) < len(shortest):
                shortest = list
    return shortest

print(find_shortest(lol))