# This function takes a list of product prices and number of sales of the product. It sums up the total revenue of those sales.

# Here's an example input for the total_revenue function with one product in it:

# products_sold = [
#     {
#         "sales": 10,
#         "price": 6.5,
#     },
# ]
# The total revenue for the example above would be 10 x 6.5 = 65.

# Here's an example with two items:


# The total revenue for the example above is: (11 x 9) + (5 x 1.5) = 106.5.

# If the list is empty, then the total revenue should be 0.

def total_revenue(product_sales):
    total_revenue = 0
    for product in product_sales:
        total_revenue += product["sales"] * product["price"]
    return total_revenue

products_sold = [
    { "sales": 1, "price": 1,   },
    { "sales": 5,  "price": 1.5, },]  ## using the function for only 1 dictionary in the list
result = products_sold[1]
print(total_revenue([result]))