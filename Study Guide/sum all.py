# Implement the function sum_all(), which returns the sum of all of its arguments.

# Sample inputs/outputs:

# numbers	output
# (none)	0
# 9	9
# 2, 3, 4, 5	
def sum_all(numbers):
    if len(numbers) == 0:
        return 0
    else:
        return sum(numbers)
print(sum_all([1,2,3,4,5]))

