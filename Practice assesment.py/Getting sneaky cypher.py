def shift_cipher(message, shift):
    if len(message) == 0:
        return ""
    result = ""
    for char in message:
        value = ord(char)
        value += shift
        result += chr(value)
    return result