shipment = [
    {
        "product_name": "beans",
        "product_weight_pounds": 2,
        "quantity": 5,
    },
    {
        "product_name": "rice",
        "product_weight_pounds": 1.5,
        "quantity": 7,
    },
]

def find_shipment_weight(shipment):
    total_weight = 0
    for values in shipment:
        total_weight += values["product_weight_pounds"] * values["quantity"]
    return total_weight
   