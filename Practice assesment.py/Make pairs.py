# Please complete the pair_up function below so that it takes a list of items and returns a list of pairs of consecutive items. Here's an example:

# input = [1,2,3,4,5,6,7]

# pair_up(input)  # Returns [[1,2], [3,4], [5,6]]
# Note: since there were an odd number of items, the 7 didn't have a partner to pair with, so it was excluded.

# Think through this problem. It may seem complex, but you can do this.

# What gets returned for an empty list?
# Can you do this with a single if statement?
# Can you do this with a for loop?

def pair_up(items):
 def pair_up(items):
    # your code here
    pairs = []                                  #-hide
    pair0 = items[::2]                          #-hide
    pair1 = items[1::2]                         #-hide
    for idx in range(len(pair1)):               #-hide
        pairs.append([pair0[idx], pair1[idx]])  #-hide
    return pairs                                #-hide
 
 def pair_up(items):
    pairs = []
    # Loop through the indexes of the items
    for index, item in enumerate(items):
        # If the index + 1 is less than the length
        # if the index is even
        if index % 2 == 0:
            if index + 1 < len(items):
                # Make a pair
                pair = [items[index], items[index + 1]]
                # Append the pair
                pairs.append(pair)
    return pairs
 
 def pair_up(items):
    pairs = []
    # Looping through 2 at a time using range
    for index in range(0, len(items), 2):
        # If index + 1 is inside the list
        if index + 1 < len(items):
            # add the current index item and the index + 1 item to the pair
            pair = [items[index], items[index + 1]]
            # Append the pair to the pairs list
            pairs.append(pair)

    return pairs
 
 