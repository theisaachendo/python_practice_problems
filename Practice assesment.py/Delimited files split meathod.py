# This function must take a string that's composed of values that are separated by some "delimiter" character, like a comma. It will return an list of the values in the string with the delimiter removed. Here is an example:

# input = "1,2,3,4"
# read_delimited(input)  # --> ["1","2","3","4"]
# If input is an empty string, the result should be a list with a single empty string in it : [""]

# Please refer to the split method for strings to see how to do this.

# Please complete the read_delimited function here.

# line	separator	expected output
# ""	","	[""]
# "a,b,c"	","	["a","b","c"]
# "bat##cat##rat"	"##"	["bat", "cat", "rat"]
# def read_delimited(line, separator):
def count_entries(line, separator):
    if not line:
        return 0
    result = 1
    for letter in line:
        if letter == separator:
            
            result += 1
    return result