# This function returns True if number is an integer multiple of base where number and base are both integers.

# If number is an integer multiple of base, then number/base is an integer.

# Here are some examples:

def is_multiple_of(number, base):
    if int(number) != number:
        return False
    elif int(base) != base:
        return False
    return number % base == 0 