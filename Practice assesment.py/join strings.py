def join_strings(strings, separator):
    if len(strings) == 0:
        return ""
    
    result = strings[0]
    for string in strings[1:]:
        result += separator + string
    
    return result
print(join_strings("a,b,c,d", "_")) 

