def monty(swaps):
    left = "notqueen"
    middle = "queen"
    right =  "notqueen" 
    for move in swaps:
        if move == "L":
            placeholder = left
            left = middle
            middle = placeholder
            return left
        elif move == "R":
            placeholder = right
            right = middle
            return right
        else:
            return middle
    if left == "queen":
        return left
    elif right == "queen":
        return right
    else:
        return middle
print(monty("L,O,L,R,O,L"))


