# The eldest child
# Given the integers youngest_age and middle_age, complete the function calculate_eldest_age so that it returns the age of the oldest sibling.

# Constraints:

# 0 <= youngest_age <= 50
# youngest_age <= middle_age <= 50
# Here are some example inputs and return values:

# youngest_age	middle_age	Return value	Reason
# 0	5	10	The difference between the ages is 5 years
# 10	11	12	The difference between the ages is 1 year
# 5	17	29	The difference between the ages is 12 years

def calculate_eldest_age(youngest_age, middle_age):
    age_difference = middle_age - youngest_age
    eldest_age = age_difference + middle_age
    return eldest_age