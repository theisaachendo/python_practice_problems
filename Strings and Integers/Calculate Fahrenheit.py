# Given the value celsius, complete the function calculate_fahrenheit so that it returns the corresponding temperature in Fahrenheit.

# Constraint: -40 <= celsius <= 40

# Here are some example inputs and return values:

# celsius	Return value	Reason
# 0	32	0C is 32F
# 10	50	10C is 50F
# It's guaranteed that celsius will be chosen so that the corresponding value in Fahrenheit will be an exact integer.

def calculate_fahrenheit(celsius):
    fahrenheit = celsius * 9 / 5 + 32 
    f_str = f'{fahrenheit}  degrees Farhrenheit'
    return f_str
print(calculate_fahrenheit(3000))