# Given the integer num_a, complete the function yas so that it returns the word "Yas!" with that number of "a" letters in it.

# Constraint: 1 <= num_a <= 30.

# Here are some example inputs and return values:

# 1	Yas!	The word has one "a"
# 5	Yaaaaas!	The word has five "a"s
# 10	Yaaaaaaaaaas!	The word has ten "a"s


def yas(num_a):
    return "Y" + "a" * num_a + "s" + "!"

