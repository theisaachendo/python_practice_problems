# Given the following input values:

# num_cupcakes, which is how many cupcakes you have
# num_tubes_frosting, which is how many tubes of frosting you have
# tubes_per_cupcake, which is exactly how many tubes of frosting go on each cupcake
# Complete the function profit so that it returns the total dollars that you make given the following information:

# A frosted cupcake sells for $10
# A cupcake with no frosting sells for $4
# A tube of frosting by itself sells for $1
# Constraints:

# 1 <= num_cupcakes <= 100
# 10 <= num_tubes_frosting <= 50
# 1 <= tubes_per_cupcake <= 5
# Example:

# Let's say you had 10 cupcakes, 5 tubes of frosting, and it takes 2 tubes of frosting to frost a cupcake. You can frost two cupcakes. You have one tube of frosting left over.

# Item	Quantity	Selling price	Total
# Frosted cupcake	2	$10	$20
# Cupcake with no frosting	8	$4	$32
# Leftover tubes of frosting	1	$1	$1
# This means that you would make $53.

def profit(num_cupcakes, num_tubes_frosting, tubes_per_cupcake):
    frosted_cupcakes = num_tubes_frosting // tubes_per_cupcake
    un_frosted_cupcakes = num_cupcakes - frosted_cupcakes
    frosted_money = frosted_cupcakes * 10
    un_frosted_money = un_frosted_cupcakes * 4
    tubes_money = (num_tubes_frosting % tubes_per_cupcake) * 1
    total_profit = frosted_money + un_frosted_money + tubes_money
    return total_profit

print(profit(10, 5, 2)) 