# Given the integer num_fars, complete the function new_hope so that it returns the sentence, "A long time ago in a galaxy far, far away" with num_fars occurrences of the word "far".

# There should be a comma right after each "far" except for the last one.

# Constraint: 1 <= num_fars <= 6.

# Here are some example inputs and return values:

# num_fars	Return value	Reason
# 1	A long time ago in a galaxy far away...	Only one "far"
# 2	A long time ago in a galaxy far, far away...	It has two "far"s with a comma after the first one
# 4	A long time ago in a galaxy far, far, far, far away...	It has four "far"s with a comma after all but the last
# Make sure to return the exact sentence!

def new_hope(num_fars):
    sentence = "A long time ago in a galaxy "
    far = "far"
    for i in range(num_fars):
        sentence += far
        if i < num_fars - 1:
            sentence += ", "
    sentence += " away..."
    return sentence