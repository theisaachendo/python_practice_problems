# Smaller texts
# The input variable message contains the string that someone wants to send in your app. Complete the function count_words to count the number of words in the string. If there are more than 30 words, then return False. Otherwise, return True.

# A "word" is considered something that's not a space.

# If you have multiple spaces in a row, then it counts as one space.

# Spaces at the beginning of the message and the end of the message don't count.

# Constraint: The message will always have at least one word.

# Here are some example inputs and return values:

# num_os	Return value	Reason
# "my friend"	True	The message has only 2 words
# "my      friend"	True	The message has only 2 words and lots of ignored space between them
# "  my friend   "	True	The message has only 2 words and lots of ignored space at the start and end
# "a b c d e f g"	True	The message has only 7 words
# "a b a b a b a b a b a b a b a b a b a b a b a b a b a b a b a"	False	The message has 31 words

def count_words(message):
    count = 0
    words = message.split()
    for word in words:
        count += 1
    if word <= 30:
        return True
    else: 
        return False
        