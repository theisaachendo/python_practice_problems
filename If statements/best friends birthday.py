# Your best friend's birthday
# Your best friend is turning 30! You and some other friends have decided to throw a huge birthday bash! You've planned decorations, games, and music. Now, all of you need to plan is the menu that you're having catered.

# Luckily, you found a good caterer through some connections. The theme of the party is 1950s Americana. The caterer has promised a meal with an entree, a side, a dessert, and a drink. They send over the menu cards for each.

# Number	Course	Menu item	Calories

menu = {
  "entrees": [0, 522, 399, 501],
  "sides": [0, 125, 72, 125],
  "drinks": [0, 10, 8, 120],
  "dessert": [0, 222, 391, 100]
}

def calories(entree_num, side_num, drink_num, dessert_num):
    total_cals = menu["entrees"][entree_num] + menu["drinks"][drink_num] + menu["sides"][side_num] + menu["dessert"][dessert_num]
    return total_cals
print(calories(0,0,0,1))

    


